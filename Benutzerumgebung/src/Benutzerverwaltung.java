import java.util.Date;
import java.util.Scanner;

public class Benutzerverwaltung{

    public static void main(String[] args) {

        BenutzerverwaltungV10.start();

    }
}

class BenutzerverwaltungV10{
    private static Scanner tastatur = new Scanner(System.in);

    public static void start(){
        BenutzerListe benutzerListe = new BenutzerListe();
        boolean needInput = true;

        benutzerListe.insert(new Benutzer("Paula", "paula"));
        benutzerListe.insert(new Benutzer("Adam37", "adam37"));
        benutzerListe.insert(new Benutzer("Darko", "darko"));

        do {
            System.out.println("Wählen Sie bitte die gewünschte Aktion aus: \n   - Anmelden: [1]\n   - Registrieren [2]\n   - Beenden [3]");

            try {
                switch (tastatur.nextInt()) {
                    case 1 -> Anmelden(benutzerListe);
                    case 2 -> Registrieren(benutzerListe);
                    case 3 -> {
                        System.out.println(benutzerListe.select());
                        System.out.println("Beenden...");
                        return;
                    }
                    default -> System.out.println("Bitte geben Sie nur Zahlen zwischen 1 und 3 ein");
                }
            } catch (java.util.InputMismatchException e) {
                System.out.println("Bitte geben Sie nur Zahlen zwischen 1 und 3 ein.");
                tastatur = null;
                tastatur = new Scanner(System.in);
            }
        } while (true);

    }

    private static void Registrieren(BenutzerListe benutzerListe) {
        String userName = "";
        String userPassword = "";

        do {
            System.out.println("Bitte geben Sie Ihren Nutzername ein");
            userName = tastatur.next();

            if(benutzerListe.select(userName).equals("")) break;

            System.out.println("Nutzername bereits vergeben");
        } while(true);

        do {
            System.out.println("Bitte geben Sie Ihr gewünschtes Passwort ein");
            userPassword = tastatur.next();
            System.out.println("Bitte geben Sie Ihr Passwort erneut ein");

            if(userPassword.equals(tastatur.next())) break;

            System.out.println("Passwörter stimmen nicht überein, bitte verscuhen Sie es erneut");
        } while (true);

        System.out.println("Nutzer "+ userName +" wurde erstellt.");
        benutzerListe.insert(new Benutzer(userName, userPassword));
    }

    private static void Anmelden(BenutzerListe benutzerListe) {
        Scanner tastatur = new Scanner(System.in);
        System.out.println("Bitte geben Sie Ihren Nutzernamen und Ihr Passwort ein:");
        boolean passwdCorrect = false;
        String userName = "";
        String[] user = new String[2];

        System.out.println("User:");
        userName = tastatur.next();

        if(!benutzerListe.select(userName).equals("")) {
            user = benutzerListe.select(userName).split(" ");
        }

        int anzahlVersuche = 0;
        do {
            System.out.println("Passwort:");
            String passwd = tastatur.next();
            if(user[1] != null) {
                passwdCorrect = testPassword(user[1], passwd);
            }

            if(passwdCorrect) {
                System.out.println("Richtiges Passwort");
                Benutzer benutzer = benutzerListe.sucheBenutzer(userName);
                benutzer.setStatus(true);
                Date datum = new Date();
                benutzer.setLastLogin(datum);

                return;
            }

            anzahlVersuche++;
            System.out.println("Falsches Passwort. \n Versuch "+anzahlVersuche+"/3");
        } while(anzahlVersuche < 3);

        System.out.println("Falscher Name oder falsches Passwort");
    }

    // Dumme Spielerei um die Passwort abfrage zu sichern (Timing sidechannel)
    private static boolean testPassword(String dbPass, String inputPass) {
        String[] pass1 = dbPass.split("");
        String[] pass2 = inputPass.split("");

        boolean returnValue = false;
        int muell = 0;

        if(pass1.length != pass2.length) {
            for(String letter : pass2) {
                if(letter.equals("test")) {
                    muell++;
                } else {
                    muell--;
                }
            }

            return false;
        }

        for(int i = 0; i< pass1.length; i++) {
            if(pass1[i].equals(pass2[i])) {
                if(muell == 0) {
                    returnValue = true;
                }
            } else {
                returnValue = false;
                muell = 1;
            }
        }

        return returnValue;
    }
}

class BenutzerListe{
    private Benutzer first;
    private Benutzer last;

    public BenutzerListe(){
        first = last = null;
    }

    public void insert(Benutzer b){
        // Sicherheitshalber setzen wir
        // den Nachfolger auf null:
        b.setNext(null);

        if(first == null){
            first = last = b;
        }
        else{
            last.setNext(b);
            last = b;
        }
    }

    public String select(){
        String s = "";
        Benutzer b = first;

        while(b != null){
            s += b.toString() + '\n';
            b = b.getNext();
        }

        return s;
    }

    public String select(String name){
        Benutzer b = first;

        while(b != null){
            if(b.hasName(name)){
                return b.toString();
            }

            b = b.getNext();
        }

        return "";
    }

    public Benutzer sucheBenutzer(String name) {
                Benutzer b = first;

        while(b != null){
            if(b.hasName(name)){
                return b;
            }

            b = b.getNext();
        }

        return null;
    }

    public boolean delete(String name){
        // ...
        return true;
    }
}

class Benutzer{
    private String name;
    private String passwort;
    private boolean status;
    private Date lastLogin;
    private Benutzer next;

    public Benutzer(String name, String pw){
        this.name = name;
        this.passwort = pw;

        this.next = null;
    }

    public boolean hasName(String name){
        return name.equals(this.name);
    }

    public String toString(){
        String s = "";
        s += name + " ";
        s += passwort;
        return s;
    }

    public Benutzer getNext(){
        return next;
    }

    public void setNext(Benutzer b){
        next = b;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }
}


