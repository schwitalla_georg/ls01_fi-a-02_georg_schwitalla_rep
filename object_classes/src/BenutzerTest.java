public class BenutzerTest {

    public static void main(String[] args) {
        Benutzer[] nutzer = new Benutzer[5];

        nutzer[0] = new Benutzer("Peter");
        nutzer[0].setName("new Peter");
        nutzer[0].setEmail("Peter.tolle@mail.digga");
        nutzer[0].setBerechtigung("User");
        nutzer[0].setPasswort("PeterPass");

        nutzer[1] = new Benutzer("Olli");
        nutzer[1].setName("new Olli");
        nutzer[1].setEmail("Olli.tolle@mail.digga");
        nutzer[1].setBerechtigung("User");
        nutzer[1].setPasswort("OlliPass");

        nutzer[2] = new Benutzer("Dieter");
        nutzer[2].setName("new Dieter");
        nutzer[2].setEmail("Dieter.tolle@mail.digga");
        nutzer[2].setBerechtigung("User");
        nutzer[2].setPasswort("DieterPass");

        nutzer[3] = new Benutzer("Heins");
        nutzer[3].setName("new Heins");
        nutzer[3].setEmail("Heins.tolle@mail.digga");
        nutzer[3].setBerechtigung("User");
        nutzer[3].setPasswort("Heinspass");

        nutzer[4] = new Benutzer("Heinrich");
        nutzer[4].setName("new Heinrich");
        nutzer[4].setEmail("Heinrich.tolle@mail.digga");
        nutzer[4].setBerechtigung("User");
        nutzer[4].setPasswort("Heinrichpass");

        for (Benutzer benutzer : nutzer) {
            System.out.println(benutzer.getName());
            System.out.println(benutzer.getEmail());
            System.out.println(benutzer.getBerechtigung());
            System.out.println(benutzer.getPasswort());
        }
    }
}
