public class Benutzer {
    private String name;
    private String passwort;
    private String email;
    private String berechtigung;

    public Benutzer(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    public String getPasswort() {
        return this.passwort;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return this.email;
    }

    public void setBerechtigung(String berechtigung) {
        this.berechtigung = berechtigung;
    }

    public String getBerechtigung() {
        return this.berechtigung;
    }
}
