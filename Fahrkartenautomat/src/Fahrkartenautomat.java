import java.util.InputMismatchException;
import java.util.Scanner;

public class Fahrkartenautomat {
    public static void main(String[] args) {
        do {
            String[][] tickets = new String[10][2];
            tickets = fülleArray(tickets);
/*            Automat myUI = new Automat();
            myUI.showOverlay();*/

            float rückgabebetrag;
            float zuZahlenderBetrag = (float) preisBerechnung(tickets);

            // Geldeinwurf
            // -----------
            float eingezahlterGesamtbetrag = geldeinwurf(zuZahlenderBetrag);

            // Fahrscheinausgabe
            // -----------------
            //fahrscheinausgabe(tickets);

            // Rückgeldberechnung und -Ausgabe
            // -------------------------------
            rückgabebetrag = (eingezahlterGesamtbetrag - zuZahlenderBetrag);
            rueckgabeberechnung(rückgabebetrag);

            System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
                    "vor Fahrtantritt entwerten zu lassen!\n" +
                    "Wir wünschen Ihnen eine gute Fahrt.");

            System.out.println("Wollen Sie weitere Tickets kaufen? (Ja für nochmal)");
        } while (new Scanner(System.in).next().equals("Ja"));
        System.out.println("Wir wünschen Ihnen einen guten Tag");
    }

    private static String[][] fülleArray(String[][] tickets) {
        tickets[0][0] = "Einzelfahrschein Berlin AB";
        tickets[0][1] = "2.90";
        tickets[1][0] = "Einzelfahrschein Berlin BC";
        tickets[1][1] = "3.30";
        tickets[2][0] = "Einzelfahrschein Berlin ABC";
        tickets[2][1] = "3.60";
        tickets[3][0] = "Kurzstrecke";
        tickets[3][1] = "1.90";
        tickets[4][0] = "Tageskarte Berlin AB";
        tickets[4][1] = "8.60";
        tickets[5][0] = "Tageskarte Berlin BC";
        tickets[5][1] = "9.00";
        tickets[6][0] = "Tageskarte Berlin ABC";
        tickets[6][1] = "9.60";
        tickets[7][0] = "Kleingruppen-Tageskarte Berlin AB";
        tickets[7][1] = "23.50";
        tickets[8][0] = "Kleingruppen-Tageskarte Berlin BC";
        tickets[8][1] = "24.30";
        tickets[9][0] = "Kleingruppen-Tageskarte Berlin ABC";
        tickets[9][1] = "24.90";

        return tickets;
    }

    public static float geldeinwurf(float zuZahlenderBetrag) {
        float eingezahlterGesamtbetrag = 0.0f;

        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.printf("Noch zu zahlen: %.2f \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            float eingeworfeneMünze = gibPreis();
            if (eingeworfeneMünze > 2 || eingeworfeneMünze < 0.05 && eingeworfeneMünze != 0) {
                System.out.println("Bitte werfen Sie nur Münzen zwischen 5Ct und 2 Euro ein!");
            } else {
                eingezahlterGesamtbetrag += eingeworfeneMünze;
            }
        }

        return eingezahlterGesamtbetrag;
    }

    public static float preisBerechnung(String[][] tickets) {
        float zuZahlenderBetrag = 0.0f;
        int[] gewünschteTicket = new int[10];

        System.out.println("Bitte geben Sie die Nummer zu Ihrem gewühnschten Ticket ein: ");
        for(int i = 0; i < tickets.length; i++) {
            System.out.printf("Geben Sie %d ein für %s, mit dem Preis %s\n", i+1, tickets[i][0], tickets[i][1]);
        }

        gewünschteTicket[0] = gibAnzahl();
        while(gewünschteTicket[0] == 0) {
            System.out.println("Bitte geben Sie eine ganze Zahl ein, welche der Auswahl entspricht");
            gewünschteTicket[0] = gibAnzahl();
        }

        System.out.println("Bitte geben Sie weitere Tickets ein, für weiter drücken Sie bitte 0: ");
        int anzahlTickets = 0;
        while(gewünschteTicket[anzahlTickets] != 0 && anzahlTickets < 10 ) {
            anzahlTickets++;
            gewünschteTicket[anzahlTickets] = gibAnzahl();
        }

        for(int i = 0; i < anzahlTickets; i++) {
            if(gewünschteTicket[i] == 0) {
                break;
            }
            zuZahlenderBetrag += Double.parseDouble(tickets[gewünschteTicket[i]-1][1]);
        }

        return zuZahlenderBetrag;
    }

    public static int gewünschteAnzahl() {
        int anzahlTickets = 0;
        Scanner tastatur = new Scanner(System.in);

        while (anzahlTickets == 0) {
            System.out.printf("Anzahl Tickets: ");
            try {
                anzahlTickets = tastatur.nextInt();
                if (anzahlTickets > 10 || anzahlTickets <= 0) {
                    // Fände es ärgerlich für den Kunden wenn er mehr tickets haben will und auf einmal nur eins rauskommt,
                    // ist etwas unpraktisch, daher frage ich einfach nochmal nach der Anzahl
                    System.out.println("Dieser Automat kann nur zwischen 1 und 10 Tickets in einer Anfrage drucken.");
                    anzahlTickets = 0;
                } else {
                    return anzahlTickets;
                }

            } catch (InputMismatchException e) {
                System.out.println("Bitte nur eine Ganzzahl angeben, vielen Dank");
                tastatur = null;
                System.gc();
                tastatur = new Scanner(System.in);
            }
        }

        return 0;
    }

    public static void fahrscheinausgabe(int anzahlTickets) {
        if (anzahlTickets == 1) {
            System.out.println("\nFahrschein wird ausgegeben");
        } else {
            System.out.printf("\n%d Fahrscheine werden ausgegeben\n", anzahlTickets);
        }

        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");

    }

    public static void rueckgabeberechnung(float rückgabebetrag) {
        if (rückgabebetrag >= 0.0f) {
            System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO ", rückgabebetrag);
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while (rückgabebetrag > 1.99f) // 2 EURO-Münzen
            {
                System.out.println("2.00 EURO");
                rückgabebetrag -= 2.0f;
            }
            while (rückgabebetrag > 0.99f) // 1 EURO-Münzen
            {
                System.out.println("1.00 EURO");
                rückgabebetrag -= 1.0f;
            }
            while (rückgabebetrag > 0.49f) // 50 CENT-Münzen
            {
                System.out.println("0.50 EURO");
                rückgabebetrag -= 0.5f;
            }
            while (rückgabebetrag > 0.19f) // 20 CENT-Münzen
            {
                System.out.println("0.20 EURO");
                rückgabebetrag -= 0.2f;
            }
            while (rückgabebetrag > 0.09f) // 10 CENT-Münzen
            {
                System.out.println("0.10 EURO");
                rückgabebetrag -= 0.1f;
            }
            while (rückgabebetrag > 0.04f)// 5 CENT-Münzen
            {
                System.out.println("0.05 EURO");
                rückgabebetrag -= 0.05f;
            }
        }

    }

    public static float gibPreis() {
        Scanner tastatur = new Scanner(System.in);
        try {
            float zuZahlenderBetrag = (float) tastatur.nextDouble();
            int zweiteStelle = ((int) (zuZahlenderBetrag * 100)) % 10;
            if (zweiteStelle != 0 && zweiteStelle != 5) {
                zuZahlenderBetrag = 0;
                System.out.println("Ungültige Eingabe. Eingaben dürfen keine 1 oder 2 Cent Stücke enthalten.");
            }
            if(zuZahlenderBetrag<0) {
                zuZahlenderBetrag = 0;
                System.out.println("Ungültige Eingabe. Eingaben dürfen nicht negativ sein.");
            }

            return zuZahlenderBetrag;
        } catch (InputMismatchException e) {
            tastatur = null;
            System.gc();
            System.out.println("Bitte die Zahl mit einem Komma trennen (x,y)!");
        }

        return 0.0f;
    }

    private static int gibAnzahl() {
        Scanner tastatur = new Scanner(System.in);

        int tickets = -1;

        while (tickets == -1) {
            try {
                System.out.printf("\nIhre Eingabe: ");
                tickets = tastatur.nextInt();

                if(tickets > 0 || tickets < 10){
                    return tickets;
                }

                System.out.println("Bitte geben Sie eine Zahl ein, welche der Auswahl entspricht");
            } catch (InputMismatchException e) {
                tastatur = null;
                System.gc();
                System.out.println("Bitte geben Sie eine ganze Zahl ein, welche der Auswahl entspricht");
                tickets = -1;
            }
        }
        return tickets;
    }
}