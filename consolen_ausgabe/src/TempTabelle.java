public class TempTabelle {
    public static void main(String[] args) {
        System.out.printf("Fahrenheit%2s  Celsius\n", "|");
        System.out.printf("----------------------\n");
        System.out.printf("-20%9s%-0-28.8889\n", "|");
        System.out.printf("-10%9s%-0-23.3333\n", "|");
        System.out.printf("0%11s%-0-23.3333\n", "|");
        System.out.printf("20%10s%-0-6.6667\n", "|");
        System.out.printf("30%10s%-0-1.1111\n", "|");
    }
`}
