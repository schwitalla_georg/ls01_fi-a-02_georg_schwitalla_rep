import java.util.Scanner;

public class TrainingsTasks {
    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);
        // aufgabe1(myScanner);
        aufgabe2(myScanner);
    }

    public static void aufgabe1(Scanner myScanner) {
        int numberToCount = myScanner.nextInt();
        countDownWhile(numberToCount);
        System.out.println("------------------------------------------------------");
        countDownWFor(numberToCount);
        System.out.println("------------------------------------------------------");
        countUpWhile(numberToCount);
        System.out.println("------------------------------------------------------");
        countUpWFor(numberToCount);
        System.out.println("------------------------------------------------------");
        countDownRec(numberToCount);
        System.out.println("------------------------------------------------------");
        countUpRec(numberToCount);
    }

    public static void countDownWhile(int n) {
        while(n>=0) {
            System.out.println(n--);
        }
    }

    public static void countDownWFor(int n) {
        for(;n>=0;n--) {
            System.out.println(n);
        }
    }

    public static void countUpWhile(int n) {
        int up = 0;
        while(n>=up) {
            System.out.println(up++);
        }
    }

    public static void countUpWFor(int n) {
        for(int i = 0;i<=n;i++) {
            System.out.println(i);
        }
    }

    public static int countDownRec(int n) {
        System.out.println(n);
        if(n == 0) {
            return 0;
        }
        return countDownRec(n-1);
    }

    public static int countUpRec(int n) {
        int x;
        if(n>=0) {
            x = countUpRec(n-1);
            System.out.println(x+1);
        }
        return n;
    }


    public static void aufgabe2(Scanner myScanner) {
        int numberToSum = myScanner.nextInt();
        sumAWhile(numberToSum);
        System.out.println("------------------------------------------------------");
        sumBWhile(numberToSum);
        System.out.println("------------------------------------------------------");
        sumCFor(numberToSum);
        System.out.println("------------------------------------------------------");
        sumDFor(numberToSum);
    }

    public static void sumAWhile(int n) {
        int i = 1;
        String antw = "";
        int erg = 0;
        while(i<n) {
            antw = antw + i + "+";
            erg += i++;
        }
        erg += n;
        System.out.println("Summe: "+erg);
        System.out.println("Grenzwert: "+n);
        System.out.println(antw + n + " = " + erg);
    }

    public static void sumBWhile(int n) {
        int i = 1;
        String antw = "";
        int erg = 0;
        while(i<n) {
            antw = antw + 2*i + "+";
            erg += 2*i++;
        }
        erg += 2*n;
        System.out.println("Summe: "+erg);
        System.out.println("Grenzwert: "+n);
        System.out.println(antw + 2*n + " = " + erg);
    }

    public static void sumCFor(int n) {
        String antw = "";
        int erg = 0;
        for(int i = 0;i<n; i++) {
            antw = antw + (2*i+1) + "+";
            erg += (2*i+1);
        }
        erg += (2*n+1);
        System.out.println("Summe: "+erg);
        System.out.println("Grenzwert: "+n);
        System.out.println(antw +  (2*n+1) + " = " + erg);
    }

    public static void sumDFor(int n) {
        String antw = "";
        int erg = 0;
        for(int i = 0;i<=n; i++) {
            antw = antw + (2*i+1)*(-1) + "+";
            erg += (2*i+1)+(-1);
        }
        erg += -2*n;
        System.out.println("Summe: "+erg);
        System.out.println("Grenzwert: "+n);
        System.out.println(antw + "-" + 2*n + " = " + erg);
    }
}
